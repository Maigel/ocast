<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class CheckApiToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!empty(trim($request->header('token')))){
            if (User::where('id' , Auth::guard('api')->id())->exists()){
                return $next($request);
            }
        }

        return response()->json('Invalid Token!', 401);
    }
}
