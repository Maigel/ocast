<?php

namespace App\Http\Controllers;

use App\Services\UserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * @var UserService
     */
    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request): JsonResponse
    {
        $result = $this->userService->registerUser($request);
        if (!$result['status']) {
            return response()->json([
                'error' => $result['errors']
            ], 400);
        }

        return response()->json([
            'message' => 'You were successfully registered. Use your email and password to sign in.'
        ], 201);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request): JsonResponse
    {
        $result = $this->userService->loginUser($request);

        if (!$result['status']) {
            return response()->json([
                'error' => $result['errors']
            ], 403);
        }

        return response()->json([
            'token_type' => 'Bearer',
            'token' => $result['token']
        ], 200);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function usersBorrowedBooksList()
    {
        $result = $this->userService->getUsersBorrowedBooksList();
        return response()->json($result, 200);
    }
}
