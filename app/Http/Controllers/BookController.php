<?php

namespace App\Http\Controllers;

use App\Services\BookService;
use Illuminate\Http\Request;

class BookController extends Controller
{
    /**
     * @var BookService
     */
    private $bookService;

    public function __construct(BookService $bookService)
    {
        $this->bookService = $bookService;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function borrow(Request $request)
    {
        $user = $request->user();
        $books = $request->only('books');
        $result = $this->bookService->borrowBook($user, $books['books']);
        return response()->json($result, 200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function unborrow(Request $request)
    {
        $user = $request->user();
        $books = $request->only('books');
        $result = $this->bookService->unborrowBook($user, $books['books']);
        return response()->json($result, 200);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function borrowBooksList()
    {
        $result = $this->bookService->getBorrowedList();
        return response()->json($result, 200);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function allBooks()
    {
        $result = $this->bookService->getAllBooks();
        return response()->json($result, 200);
    }
}
