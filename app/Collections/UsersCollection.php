<?php

namespace App\Collections;

use App\Models\User;
use App\ViewModel\UsersViewModel;

class UsersCollection
{
    private $users = [];

    public function __construct($users)
    {
        foreach ($users as $user) {
            $this->addUser($user);
        }
    }

    /**
     * @param User $user
     */
    private function addUser(User $user): void
    {

        $this->users[] = new UsersViewModel($user);
    }

    /**
     * @return array
     */
    public function getUsers(): array
    {
        return $this->users;
    }
}
