<?php

namespace App\Collections;

use App\Models\Book;
use App\ViewModel\BookViewModel;

class BookCollection
{
    /**
     * @var array
     */
    private $books = [];

    /**
     * BorrowedBooksCollection constructor.
     */
    public function __construct($books)
    {
        foreach ($books as $book) {
            $this->addBook($book);
        }
    }

    /**
     * @param Book $book
     */
    private function addBook(Book $book): void
    {
        $this->books[] = new BookViewModel($book);
    }

    /**
     * @return array
     */
    public function getBooks(): array
    {
        return $this->books;
    }
}
