<?php

namespace App\Collections;

use App\Models\BorrowedBook;
use App\ViewModel\BorrowedBooksViewModel;

class BorrowedBooksCollection
{
    /**
     * @var array
     */
    private $books = [];

    /**
     * BorrowedBooksCollection constructor.
     */
    public function __construct($books)
    {
        foreach ($books as $book) {
            $this->addBook($book);
        }
    }

    /**
     * @param BorrowedBook $book
     */
    private function addBook(BorrowedBook $book): void
    {
        $this->books[] = new BorrowedBooksViewModel($book);
    }

    /**
     * @return array
     */
    public function getBooks(): array
    {
        return $this->books;
    }
}
