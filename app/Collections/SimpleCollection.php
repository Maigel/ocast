<?php

namespace App\Collections;

use JsonSerializable;

class SimpleCollection implements JsonSerializable
{
    /**
     * @var bool
     */
    private $status;
    /**
     * @var array
     */
    private $elements = [];

    public function __construct(array $elements, bool $status)
    {
        $this->elements = $elements;
        $this->status = $status;
    }

    public function jsonSerialize()
    {
        return [
            'success' => $this->status,
            'data' => $this->elements
        ];
    }
}
