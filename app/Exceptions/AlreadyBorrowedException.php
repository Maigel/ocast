<?php

namespace App\Exceptions;

use Exception;

class AlreadyBorrowedException extends Exception
{
    public function render()
    {
        return response()->json(['error' => __('exceptions.already')],400);
    }
}
