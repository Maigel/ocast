<?php

namespace App\Exceptions;

use Exception;

class MaxBooksException extends Exception
{
    public function render()
    {
        return response()->json(['error' => __('exceptions.max_books')],400);
    }
}
