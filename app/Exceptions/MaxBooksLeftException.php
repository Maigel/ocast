<?php

namespace App\Exceptions;

use Exception;

class MaxBooksLeftException extends Exception
{
    public function render()
    {
        return response()->json(['error' => __('exceptions.max_books_total')],400);
    }
}
