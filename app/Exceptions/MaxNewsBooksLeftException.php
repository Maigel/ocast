<?php

namespace App\Exceptions;

use Exception;

class MaxNewsBooksLeftException extends Exception
{
    public function render()
    {
        return response()->json(['error' => __('exceptions.max_news')],400);
    }
}
