<?php

namespace App\Services;

use App\Collections\SimpleCollection;
use App\Collections\UsersCollection;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class UserService
{
    public function registerUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string',
        ]);

        if ($validator->fails()) {
            return [
                'status' => false,
                'errors' => $validator->errors()
            ];
        }

        User::create([
            'name' => $request->only('name'),
            'email' => $request->only('email'),
            'password' => bcrypt($request->only('password')),
            'have_books' => 0,
            'have_news_books' => 0
        ]);

        return [
            'status' => true,
        ];
    }

    public function loginUser(Request $request)
    {
        $credentials = $request->only('email', 'password');
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email',
            'password' => 'required|string',
        ]);

        if ($validator->fails()) {
            return [
                'status' => false,
                'errors' => $validator->errors()
            ];
        }

        if (!Auth::attempt($credentials)) {
            return [
                'status' => false,
                'errors' => 'Wrong credentials',

            ];
        }
        $token = Auth::user()->createToken(config('app.name'));
        return [
            'status' => true,
            'token' => $token->accessToken
        ];
    }

    public function getUsersBorrowedBooksList(): SimpleCollection
    {
        $users = User::has('borrowedBooks')->get();
        $usersCollection = new UsersCollection($users);

        return new SimpleCollection($usersCollection->getUsers(), true);
    }
}
