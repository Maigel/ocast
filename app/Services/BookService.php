<?php

namespace App\Services;

use App\Collections\BookCollection;
use App\Collections\BorrowedBooksCollection;
use App\Exceptions\AlreadyBorrowedException;
use App\Exceptions\MaxBooksLeftException;
use App\Exceptions\MaxNewsBooksLeftException;
use App\Models\Book;
use App\Models\User;
use App\Models\BorrowedBook;
use App\Collections\SimpleCollection;
use App\Exceptions\MaxBooksException;


class BookService
{

    /**
     * @param User  $user
     * @param array $books
     * @return SimpleCollection
     * @throws AlreadyBorrowedException
     * @throws MaxBooksException
     * @throws MaxBooksLeftException
     * @throws MaxNewsBooksLeftException
     */
    public function borrowBook(User $user, array $books): SimpleCollection
    {
        if (count($books) > 5) {
            throw new MaxBooksException();
        }

        $maxBooksToBorrow = (int)env('MAX_BOORKS_TO_BORROW');
        $maxNewsBooksToBorrow = (int)env('MAX_NEWS_BOORKS_TO_BORROW');

        $newsBooksToBorrow = Book::whereIn('id', $books)->newsCategory()->pluck('id')->toArray();

        if ($user->have_books + count($books) + count($newsBooksToBorrow) > $maxBooksToBorrow) {
            throw new MaxBooksLeftException();
        }

        $alreadyBorrowedBooks = BorrowedBook::whereIn('book_id', $books)->pluck('id')->toArray();

        if (count($alreadyBorrowedBooks) > 0) {
            throw new AlreadyBorrowedException();
        }

        if ($user->have_news_books + count($newsBooksToBorrow) > $maxNewsBooksToBorrow) {
            throw new MaxNewsBooksLeftException();
        }

        foreach ($books as $book) {
            if (in_array($book, $newsBooksToBorrow)) {
                $user->increment('have_news_books');
            } else {
                $user->increment('have_books');
            }
            BorrowedBook::create([
                'user_id' => $user->id,
                'book_id' => $book,
                'return_date' => date('Y-m-d', strtotime("+30 days"))
            ]);
        }
        return new SimpleCollection(['massage' => 'Books borrowed'], true);
    }

    /**
     * @param User  $user
     * @param array $books
     * @return SimpleCollection
     */
    public function unborrowBook(User $user, array $books): SimpleCollection
    {
        $user->borrowedBooks()->whereIn('book_id', $books)->delete();
        $newsBooksToUnborrow = Book::whereIn('id', $books)->newsCategory()->count();
        $user->have_books = $user->have_books - (count($books) - $newsBooksToUnborrow);
        $user->have_news_books = $user->have_news_books - $newsBooksToUnborrow;
        $user->save();

        return new SimpleCollection(['massage' => 'Books unborrowed'], true);
    }

    /**
     * @return SimpleCollection
     */
    public function getBorrowedList(): SimpleCollection
    {
        $borrowedBooks = BorrowedBook::with('book')->with('user')->get();
        $borrowedBooksCollection = new BorrowedBooksCollection($borrowedBooks);

        return new SimpleCollection($borrowedBooksCollection->getBooks(), true);
    }

    /**
     * @return SimpleCollection
     */
    public function getAllBooks(): SimpleCollection
    {
        $books = Book::get();
        $books = new BookCollection($books);
        return new SimpleCollection($books->getBooks(), true);
    }
}
