<?php

namespace App\ViewModel;

use App\Models\Book;
use JsonSerializable;

class BookViewModel implements JsonSerializable
{
    /**
     * @var string
     */
    private $bookName;

    /**
     * @var string
     */
    private $category;

    public function __construct(Book $book)
    {
        $this->bookName = $book->book_name;
        $this->category = $book->category->name;
    }

    public function jsonSerialize()
    {
        return [
            'name' => $this->bookName,
            'category' => $this->category
        ];
    }
}
