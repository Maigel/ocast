<?php

namespace App\ViewModel;

use App\Models\User;
use JsonSerializable;

class UsersViewModel implements JsonSerializable
{
    /**
     * @var string
     */
    private $userName;

    /**
     * @var string
     */
    private $userEmail;

    /**
     * @var array
     */
    private $borrowedBooks = [];

    public function __construct(User $user)
    {

        $this->userName = $user->name;
        $this->userEmail = $user->email;
        foreach ($user->borrowedBooks as $borrowedBook) {
            $this->borrowedBooks[] = new BookViewModel($borrowedBook->book);
        }
    }

    public function jsonSerialize()
    {
        return [
            'user'  => $this->userName,
            'email' => $this->userEmail,
            'books' => $this->borrowedBooks,
        ];
    }
}
