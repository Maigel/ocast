<?php

namespace App\ViewModel;

use App\Models\BorrowedBook;
use JsonSerializable;

class BorrowedBooksViewModel implements JsonSerializable
{

    /**
     * @var string
     */
    private $bookName;

    /**
     * @var string
     */
    private $wasBorrowed;

    /**
     * @var string
     */
    private $returnDate;

    /**
     * @var string
     */
    private $userName;

    /**
     * BorrowedBooksViewModel constructor.
     */
    public function __construct(BorrowedBook $book)
    {
        $this->bookName = $book->book->book_name;
        $this->wasBorrowed = $book->created_at;
        $this->returnDate = $book->return_date;
        $this->userName = $book->user->name;
    }

    public function jsonSerialize()
    {
        return [
            'book' => $this->bookName,
            'borrowed' => $this->wasBorrowed,
            'will_be_returned' => $this->returnDate,
            'borrowed_by' => $this->userName
        ];
    }
}
