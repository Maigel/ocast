<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{

    const NEWS_CATEGORY_NAME = 'news';

    public $timestamps = false;

    /**
     * Relation to Category
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * Relation to BorrowedBook
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function borrowedBooks()
    {
        return $this->hasOne(BorrowedBook::class);
    }

    public function scopeNewsCategory($query)
    {
        return $query->whereHas('category', function (Builder $query) {
            $query->where('name', '=', self::NEWS_CATEGORY_NAME);
        });
    }
}
