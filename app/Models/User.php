<?php

namespace App\Models;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    protected $fillable = ['name', 'email', 'password', 'have_books', 'have_news_books'];

    /**
     * Relation to BorrowedBooks
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function borrowedBooks()
    {
        return $this->hasMany(BorrowedBook::class);
    }
}
