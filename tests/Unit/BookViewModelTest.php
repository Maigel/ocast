<?php

namespace Tests\Unit;

use App\Models\Book;
use App\ViewModel\BookViewModel;
use Tests\TestCase;

class BookViewModelTest extends TestCase
{
    /**
     * @test
     */
    public function shouldSerialize(): void
    {
        $book = factory(Book::class)->make([
            'book_name' => 'bookName',
            'category_id' => 1
        ]);
        $fixture = new BookViewModel($book);
        $serialized = ['name' => 'bookName', 'category' => 'News'];
        $this->assertEquals($fixture->jsonSerialize(), $serialized);
    }
}
