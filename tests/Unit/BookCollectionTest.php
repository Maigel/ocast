<?php

namespace Tests\Unit;

use App\Collections\BookCollection;
use App\Models\Book;
use App\ViewModel\BookViewModel;
use Tests\TestCase;

class BookCollectionTest extends TestCase
{

    /**
     * @test
     */
    public function shouldCreateCollection()
    {
        $books = factory(Book::class, 3)->make([
            'book_name' => 'bookName',
            'category_id' => 1
        ]);
        $fixture = new BookCollection($books);
        $serialized = [];
        foreach ($books as $book) {
            $serialized[] = new BookViewModel($book);
        }
        $this->assertEquals($fixture->getBooks(), $serialized);
    }

}
