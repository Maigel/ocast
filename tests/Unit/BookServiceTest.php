<?php

namespace Tests\Unit;

use App\Collections\SimpleCollection;
use App\Exceptions\AlreadyBorrowedException;
use App\Exceptions\MaxBooksException;
use App\Exceptions\MaxBooksLeftException;
use App\Exceptions\MaxNewsBooksLeftException;
use App\Models\User;
use App\Services\BookService;
use Tests\TestCase;

class BookServiceTest extends TestCase
{
    /**
     * @var BookService
     */
    protected $fixture;


    /**
     * @var mixed
     */
    protected $userModel;


    public function setUp(): void
    {
        parent::setUp();
        $this->fixture = new BookService();
        $this->userModel = factory(User::class)->make();
    }

    /**
     * @test
     */
    public function shouldCreate(): void
    {
        self::assertInstanceOf(BookService::class, $this->fixture);
    }

    /**
     * @test
     */
    public function shouldGetListOfBooks(): void
    {
        self::assertInstanceOf(SimpleCollection::class, $this->fixture->getAllBooks());
    }

    /**
     * @test
     */
    public function shouldBorrowBook(): void
    {
        $actual = $this->fixture->borrowBook($this->userModel, [0,1,2]);
        $collection = new SimpleCollection(['massage' => 'Books borrowed'], true);
        $this->assertEquals($collection, $actual);
    }

    /**
     * @test
     */
    public function shouldThrowMaxBooksExceptionTest(): void
    {
        self::expectException(MaxBooksException::class);
        $this->fixture->borrowBook($this->userModel, [0,1,2,3,4,5]);
    }

    /**
     * @test
     */
    public function shouldThrowMaxBooksLeftExceptionTest(): void
    {
        $this->userModel->have_books = 3;
        self::expectException(MaxBooksLeftException::class);
        $this->fixture->borrowBook($this->userModel, [0,1,2,3,4]);
    }

    /**
     * @test
     */
    public function shouldThrowAlreadyBorrowedException(): void
    {
        self::expectException(AlreadyBorrowedException::class);
        $this->fixture->borrowBook($this->userModel, [0,1,2]);
    }

    /**
     * @test
     */
    public function shouldThrowMaxNewsBooksLeftException(): void
    {
        $this->userModel->have_news_books = 2;
        self::expectException(MaxNewsBooksLeftException::class);
        $this->fixture->borrowBook($this->userModel, [3]);
    }

    /**
     * @test
     */
    public function shouldUnborrowBook(): void
    {
        $this->userModel->have_books = 3;
        $this->userModel->have_news_books = 1;
        $actual = $this->fixture->unborrowBook($this->userModel, [0,1,2]);
        $collection = new SimpleCollection(['massage' => 'Books unborrowed'], true);
        $this->assertEquals($collection, $actual);
    }

    /**
     * @test
     */
    public function shouldReturnBorrowedBooksList(): void
    {
        $list = $this->fixture->getBorrowedList();
        self::assertInstanceOf(SimpleCollection::class, $list);
    }

    /**
     * @test
     */
    public function shouldReturnBooksList(): void
    {
        $list = $this->fixture->getAllBooks();
        self::assertInstanceOf(SimpleCollection::class, $list);
    }

    public function tearDown(): void
    {
        parent::tearDown();
        $this->userModel->delete();
    }
}
