<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('token', 'auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('check', 'UserController@check')->middleware('token', 'auth:api');

Route::post('register', 'UserController@register');

Route::post('login', 'UserController@login');

Route::group(['middleware' => ['token', 'auth:api']], function () {
    Route::post('borrow', 'BookController@borrow');
    Route::post('unborrow', 'BookController@unborrow');
    Route::get('borrow_list', 'BookController@borrowBooksList');
    Route::get('users_list', 'UserController@usersBorrowedBooksList');
    Route::get('books_list', 'BookController@allBooks');
});
