<?php

return [
    'already' => 'Some books already borrowed',
    'max_news' => 'Cannot borrow more books from News category.',
    'max_books' => 'You cannot borrow more then 5 books!',
    'max_books_total' => 'Cannot borrow so many books'
];
