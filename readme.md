## Ocast test tast


#### Step 1
Clone this repo 

    git clone https://Maigel@bitbucket.org/Maigel/ocast.git ocast
    
#### Step 2
install dependences  

    composer install 
    
**Note! As project done using Laravel 6, php7.2 or greater required. Homestead vagrant box looks like a best solution** 

### Step 3
Install Laravel passport

    php artisan passport:install
    

### Step 4
Run migrations

     php artisan migrate
    

### Step 5
Run seeders to fill the database

    php artisan db:seed
    
####  Step 6

Import request collection file (Ocast.postman_collection.json) to your postman to test endpoints
