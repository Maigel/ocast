<?php
/** @var \Illuminate\Database\Eloquent\Factory $factory */
use Faker\Generator as Faker;
use App\Models\Book;

$factory->define(Book::class, function (Faker $faker) {
    return [
        'book_name' => $faker->name,
        'category_id' => random_int(1,4)
    ];
});
