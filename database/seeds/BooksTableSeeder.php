<?php

use Illuminate\Database\Seeder;
use App\Models\Book;


class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for ($i = 0; $i <=10; $i++) {
            $model = new Book;
            $model->book_name = $faker->name;
            $model->category_id = random_int(1,4);
            $model->save();
        }
    }
}
